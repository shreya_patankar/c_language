/*3. Write a program to find whether the given processor is little endian or big endian.
*/

#include <stdio.h>
#include <stdbool.h>


int main()
{
    int num = 0x1;
    char *c;
    c = (char *) &num;
    if(c)
    printf("little endian");
    else
    printf("Big endian");
}