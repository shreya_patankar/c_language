/*Question 1
1. Develop a C program to add two operands and store the result in one of the operand
using addition assignment operator.
*/

#include <stdio.h>

int add_numbers(int a, int b)
{
    return a+b;
}

int main()
{
    int a=6, b=-10, sum=0;
    sum = add_numbers(a,b);

    printf("the sum is %d", sum);
}
