/*
Basic Data Types
1. Develop a C program to calculate simple interest using the formula I=PTR/100. Display
Interest with two digit precision after decimal point
*/

#include <stdio.h>

float round_upto_2_places(float *value)
{
    float v=(int)(*value * 100 + 0.5);
    return (float)v/100;

}
float simple_interest_calculation(float principal_amount,float rate_of_interest, int time_in_months)
{
    float simple_interest = (principal_amount * time_in_months * rate_of_interest)/100;
        float round_places = round_upto_2_places(&simple_interest);
        return simple_interest;

}

int main()
{
    float principal=10000.87;
    float rate=8.7;
    int months=7;
    float s_interest=simple_interest_calculation(principal,rate,months);
    printf("simple interest is %.2f", s_interest); 
}