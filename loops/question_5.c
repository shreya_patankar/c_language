/*
5.Develop a C program to find sum of all odd numbers upto N using while loop.
*/

#include <stdio.h>

int main()
{
    int n, sum=0;

    printf("enter the value of N:\n");
    scanf("%d",&n);
    int i=1;
    while(i<=n)
    {
        if(!(i%2==0))
        
        {
            sum=sum+i;
        }
        i++;
    }
    printf("the sum of all odd numbers till n is %d\n",sum);

}
