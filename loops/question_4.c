/*
4. Develop a C program to find factorial of a number N using for loop.
*/
#include <stdio.h>

int main()
{
    int n, fact=1;

    printf("enter the value of N:\n");
    scanf("%d",&n);
    if(n==0 || n==1)
    fact =1;
    else
    {
    for(int i=1;i<=n;i++)
    {
            fact=fact*i;
    }
    }
    printf("the factorial of n numbers is %d\n",fact);

}