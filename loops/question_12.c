/*
12. Write a program to find the sum of digits of a given number.
*/

#include<stdio.h>

int sum_of_digits(int n)
{
    int sum=0,r;
    while(n)
    {
        r=n%10;
        sum=sum+r;
        n=n/10;
    }
    return sum;
}

int main()
{
    int n,summ=0;
    printf("enter the number");
    scanf("%d",&n);
    summ=sum_of_digits(n);
    printf("the number is %d",summ);
}