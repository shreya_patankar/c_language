/*
Write a program to print ASCII values of upper case and lower case alphabets and digits
(A-Z, a-z and 0-9).
*/
#include <stdio.h>

int print_ascii_values(char input)
{
    if(input >='a'&& input<='z')
    {
        printf("the ascii value is %d\n",input);
    }
    else if(input >='A'&& input<='Z')
    {
        printf("the ascii value is %d\n",input);
    }
    else if(input >='0'&& input<='9')
    {
        printf("the ascii value is %d\n",input);
    }
    else
    printf("char not in range\n");
}

int main()
{
    printf("enter the input:\n");
    char i;
    scanf("%c",&i);
    print_ascii_values(i);
}