/*
9. Generate the following pyramid of numbers using nested loops

*/

#include <stdio.h>

void print_pattern(int n_lines)
{
    int i,j,k,l;

    for(i=1; i<=n_lines;i++)
    {
        for(j=i;j<n_lines;j++)
        {
            printf("*");
        }
        for(k=i ;k>=1 ;k--)
        {
            printf("%d",k);
        }
         for(l=2 ;l<=i ;l++)
        {
            printf("%d",l);
        }
        printf("\n");
    }
}

int main()
{
    int num_lines =0;
    printf("enter the number of lines");
    scanf("%d",&num_lines);
    print_pattern(num_lines);
}


