/*
8. Write a program to find whether given number is palindrome or not.
*/



#include <stdio.h>
#include<stdbool.h>
bool is_palindrome(int number)
{
    bool flag= false;
    int r=0, rev_number=0,o_number=number;
    while(number)
    {
        r=number%10;
        rev_number=rev_number*10+r;
        number=number/10;
    }
    
    if(o_number==rev_number)
    {
        flag=true;
    }
    return flag;
}

int main()
{
    int number=1261;
    if(is_palindrome(number))
        printf("number is palindrome");
    else
        printf("number is not palindrome");

}