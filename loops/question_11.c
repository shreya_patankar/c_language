/*
11. Write a program to print all the prime numbers in the first N numbers.
*/

#include <stdio.h>

void print_prime_numbers_upto(int n)
{
    int count=0;
  for(int i=1;i<=n;i++)
  {
      for(int j=1;j<=n;j++)
      {
          if(i%j==0)
          {
              count++;
             
          }
      }
      if(count==2)
      printf("%d\n",i);
      count=0;
  }  
}

int main()
{
    int n=0;
    printf("enter the value of n");
    scanf("%d",&n);
    print_prime_numbers_upto(n);
}