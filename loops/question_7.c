/*
Write a Program to find if a given number is Armstrong number. Hint: (153 = 1^3 + 5^3 +
3^3)
*/
#include <stdio.h>
#include<stdbool.h>
bool is_armstrong(int number)
{
    bool flag= false;
    int r=0, sum=0,o_number=number;
    while(number)
    {
        r=number%10;
        sum=sum+r*r*r;
        number=number/10;
    }
    
    if(o_number==sum)
    {
        flag=true;
    }
    return flag;
}

int main()
{
    int number=157;
    if(is_armstrong(number))
        printf("number is armstrong") ;
    else
        printf("number is not armstrong");

}