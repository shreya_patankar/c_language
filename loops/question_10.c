/*
10. Write a program to search for an element in a given list of elements. Use break
statement.
*/

#include <stdio.h>
#include <stdbool.h>
#define SIZE 10
bool find_element(int array[],int key)
{
   bool flag = false;
    for(int i=0;i<SIZE;i++)
    {
        if(array[i]==key)
        {
        flag = true;
        break;
        }
    }
    return flag;
}



int main()
{
    int arr[SIZE]={4,6,2,3,7,9,1,8,11,12};
    if(find_element(arr,60))
    printf("the key found");
    else
    printf("key not found");
}